import logging
import torch
from torch import nn
from torch.utils.data import Dataset, DataLoader
from torch.nn.utils.rnn import pad_packed_sequence
from tqdm import tqdm

from AMAL_tp5 import bcolors
from AMAL_tp7_datasets import VocabularyTagging, TaggingDataset
from torch_utils import NN, validation
from rnn import ps_elementwise_apply

logging.basicConfig(level=logging.INFO)

def display(words_vocab, tags_vocab, tokens, tags, tags_pred):
    to = ['TOKENS']    + [words_vocab[i] for i in pad_packed_sequence(tokens   )[0][:,0]]
    ta = ['True TAGS'] + [ tags_vocab[i] for i in pad_packed_sequence(tags     )[0][:,0]]
    tp = ['Pred TAGS'] + [ tags_vocab[torch.argmax(i,dim=0)] for i in pad_packed_sequence(tags_pred)[0][:,0]]
    l0 = len(max(to, key=len))
    l1 = len(max(ta, key=len))
    s = '{{:{}}} {{:{}}} {{}}'.format(l0,l1)
    for x in zip(to,ta,tp):
        if x[1]!=x[2]:
            print((bcolors.FAIL+s+bcolors.ENDC).format(*x))
        else:
            print(s.format(*x))

class TaggingNet(nn.Module):
    def __init__(self, inDim, embedDim, hiddenDim, fc_layers, outDim):
        super().__init__()
        self.hiddenDim = hiddenDim

        self.embedding = nn.Embedding(inDim, embedDim)
        self.rnn = nn.GRU(input_size=embedDim, hidden_size=hiddenDim, bidirectional=True)
        self.fc = NN(hiddenDim*2, outDim, fc_layers) # *2 because bidirectional

    def forward(self, x):      ## x : pack_sequence
        h = - torch.ones((2, x.batch_sizes[0], self.hiddenDim)) # 2 because bidirectional
        x = ps_elementwise_apply(self.embedding, x)
        x, _ = self.rnn(x, h)
        x = ps_elementwise_apply(self.fc, x)
        return x


if __name__=='__main__':
    from torch.utils.tensorboard import SummaryWriter

    logging.info("Loading datasets...")
    from datamaestro import prepare_dataset
    ds = prepare_dataset('org.universaldependencies.french.gsd')
    words = VocabularyTagging(True)
    tags = VocabularyTagging(False)
    train_data = TaggingDataset(ds.files["train"], words, tags, True)
    dev_data = TaggingDataset(ds.files["dev"], words, tags, True)
    test_data = TaggingDataset(ds.files["test"], words, tags, False)
    logging.info("Vocabulary size: %d", len(words))

    train_batchsize = 100
    test_batchsize  = 300
    train_dataloader = DataLoader(train_data, collate_fn=TaggingDataset.collate, shuffle=True, batch_size=train_batchsize)
    test_dataloader  = DataLoader( test_data, collate_fn=TaggingDataset.collate, shuffle=True, batch_size= test_batchsize)

    inDim    = len(words)
    outDim   = len(tags)
    embedDim = 20
    hiddenDim = 30#2*outDim
    fc_layers = [25] #[2*outDim]
    print('inDim={}, outDim={}'.format(inDim, outDim))
    net = TaggingNet(inDim=inDim, embedDim=embedDim, hiddenDim=hiddenDim, outDim=outDim, fc_layers=fc_layers)
    optimizer = torch.optim.Adam(params=net.parameters()) #, lr=0.005)

    loss_fun = nn.CrossEntropyLoss()

    # x,yt = next(iter(train_dataloader))
    # y = net(x)
    # display(words, tags, x,yt,y)
    # print(validation(net, test_dataloader, loss_fun))

    ## display params :
    N = 10000
    loss_freq      = 1 # in number of epochs
    histogram_freq = 1
    test_freq      = 3
    display_freq   = 1

    ## Savers :
    writer = SummaryWriter()

    ## Training loop :
    for i in tqdm(range(N), desc='epochs'):
        loss_train = 0.
        num_batchs = 0.
        for j, (x, y_target) in enumerate(tqdm(train_dataloader, desc='train')):
        ## train (one epoch) :
            optimizer.zero_grad()
            net.zero_grad()
            y = net(x)
            loss = loss_fun(y.data, y_target.data)
            loss.backward()
            optimizer.step()
            loss_train += loss.item()
            num_batchs += 1

        if i%display_freq==0:
            display(words, tags, x, y_target, y)

        if i%loss_freq==0:
            loss_train /= num_batchs
            writer.add_scalars('loss', {'train': loss_train}, i)

        ## test network :
        if i%test_freq==0:
            loss_test, accuracy_test = validation(net, test_dataloader, loss_fun)
            writer.add_scalars('loss',     {'test':     loss_test}, i)
            writer.add_scalars('accuracy', {'test': accuracy_test}, i)

    writer.close()
