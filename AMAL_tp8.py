import torch
import torch.nn as nn
from torch.nn import functional as F
import numpy as np
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import Dataset, DataLoader, random_split
from tqdm import tqdm
from datamaestro import prepare_dataset

from AMAL_tp3 import MyDataset
from torch_utils import NN, validation, accuracy, entropie


## TODO : logger les histograms des sorties de chaque couche

def apply(layer, x):
    if layer is None:
        return x
    return layer(x)

class RegularizedNN(nn.Module):
    def __init__(self, inSize, outSize, layers=[], batchNorm=False, layerNorm=False, dropout=None):
        super().__init__()
        self.batchNorms = nn.ModuleList([])
        self.layerNorms = nn.ModuleList([])
        self.dropouts   = nn.ModuleList([])
        self.layers     = nn.ModuleList([])
        for x in list(layers)+[outSize]:
            self.batchNorms.append(nn.BatchNorm1d(inSize) if batchNorm else None)
            self.layerNorms.append(nn.LayerNorm(  inSize) if layerNorm else None)
            self.dropouts.append(  nn.Dropout(p=dropout)  if dropout is not None else None)
            self.layers.append(nn.Linear(inSize, x))
            inSize = x
    def forward(self, x, store_outputs=False):
        self.outputs = [] if store_outputs else None
        for i in range(len(self.layers)):
            x = apply(self.batchNorms[i], x)
            x = apply(self.layerNorms[i], x)
            x = apply(  self.dropouts[i], x)

            x = self.layers[i](x)
            if store_outputs:
                self.outputs.append(x)

            if i!=len(self.layers):
                x = torch.nn.functional.leaky_relu(x)

        return x


if __name__=='__main__':

    regularisations = [
        #'dropout',
        #'batchNorm',
        #'layerNorm',
        'l2',
        #'l1',
    ]

    batch_size = 200
    lambda1 = 2e-4
    lambda2 = 2e-3
    dropout = .4

    ## Dataset
    prepared_dataset = prepare_dataset('com.lecun.mnist')
    data         = MyDataset(prepared_dataset, 'train/images', 'train/labels')
    dataset_test = MyDataset(prepared_dataset,  'test/images',  'test/labels')
    l = len(data)
    ll = [l//20, l//20]
    torch.manual_seed(12)
    dataset_train, dataset_val, _ = random_split(data, ll+[l-sum(ll)])
    train_dataloader = DataLoader(dataset_train, shuffle=True,  batch_size=batch_size)
    val_dataloader   = DataLoader(dataset_val,   shuffle=False, batch_size=len(dataset_val))
    test_dataloader  = DataLoader(dataset_test,  shuffle=False, batch_size=len(dataset_test))
    print('\ndatasets lengths :\ttrain={}, val={}, test={}\n'.format(len(dataset_train), len(dataset_val), len(dataset_test)))

    ## net params :
    inDim = 784
    outDim = 10
    layers = [100]*3
    if 'dropout' not in regularisations:
        dropout = None

    net = RegularizedNN(inDim, outDim, layers,
        batchNorm=('batchNorm'in regularisations),
        layerNorm=('layerNorm'in regularisations),
        dropout=dropout )
    optimizer = torch.optim.Adam(params=net.parameters(), lr=1e-4)
    loss_fun = nn.CrossEntropyLoss()


    # x,yt = next(iter(train_dataloader))
    # y=net(x)
    # print(x.shape, y.shape, yt.shape)
    # print(x, y, yt)
    # l = loss_fun(y,yt)
    # print(x,y,yt,l)

    ## display params :
    N = 1000
    loss_freq      = 10 # in number of epochs
    histogram_freq = 30
    test_freq      = 10

    ## Savers :
    writer = SummaryWriter(comment='_'+('_'.join(regularisations)))

    ## Training loop :
    for i in tqdm(range(N), desc='epochs'):
        loss_train, num_batchs, acc_train, num_elem = [torch.zeros(()) for _ in range(4)]
        for j, (x, y_target) in enumerate(tqdm(train_dataloader, desc='train')):
        ## train (one epoch) :
            optimizer.zero_grad()
            net.zero_grad()
            net.train()
            y = net(x, store_outputs=(i%histogram_freq==0))
            loss = loss_fun(y, y_target)
            if 'l1' in regularisations or 'l2' in regularisations:
                params = torch.cat([x.weight.view(-1) for x in net.layers])
                if 'l1' in regularisations:
                    loss += lambda1 * torch.norm(params, 1)
                if 'l2' in regularisations:
                    loss += lambda2 * torch.sum(params**2)
            loss.backward()
            optimizer.step()
            acc, total = accuracy(y, y_target)
            loss_train += loss
            num_batchs += 1
            acc_train  += acc
            num_elem   += total

        if i%loss_freq==0:
            writer.add_scalars('loss',     {'train': loss_train/num_batchs}, i)
            writer.add_scalars('accuracy', {'train': acc_train/num_elem},    i)

        ## histograms :
        if i%histogram_freq==0:
            for j,(l,o) in enumerate(zip(net.layers, net.outputs)):
                writer.add_histogram('weight/layer_'+str(j), l.weight,      i)
                writer.add_histogram(  'grad/layer_'+str(j), l.weight.grad, i)
                writer.add_histogram(  'output/layer_'+str(j),o, i)
                writer.flush()


        ## test network :
        if i%test_freq==0:
            with torch.no_grad():
                x, y_target = next(iter(val_dataloader))
                net.eval()
                y = net(x)
                loss_test = loss_fun(y, y_target)
                acc, total = accuracy(y, y_target)
            writer.add_scalars('loss',     {'test': loss_test}, i)
            writer.add_scalars('accuracy', {'test': acc.float()/total}, i)
            writer.add_scalars('entropie', {'test': torch.mean(entropie(y, dim=1))}, i)

    writer.close()
