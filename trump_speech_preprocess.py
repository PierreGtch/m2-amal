import array
import csv
import gzip
import logging
import re
import shutil
import subprocess
import sys
from collections import namedtuple
from pathlib import Path
from tqdm import tqdm
import pickle as pkl

import click
import sentencepiece as spm
import torch

from datamaestro import Dataset, prepare_dataset

logging.basicConfig(level=logging.INFO)


MAINDIR = Path(__file__).parent
RE_PUNCT = r'[.?!]\s*'
BOS_ID = 1
EOS_ID = 2


def datareader(path: Path):
    with open(path, "rt", encoding="utf-8", errors='ignore') as fp:
        for l in fp:
            for s,p in zip(re.split(RE_PUNCT,l), re.findall(RE_PUNCT,l)):
                yield s+p[0]

def cleanup(src, target):
    if not target.is_file():
        logging.info("Creating the text data file from %s", src)
        target_tmp = target.with_suffix(".tmp")
        with target_tmp.open("wt") as out:
            for s in datareader(src):
                out.write(s)
                out.write("\n")

        shutil.move(target_tmp, target)

class Encoder:
    def __init__(self, model_path):
        self.tokenizer = spm.SentencePieceProcessor()
        self.tokenizer.Load(model_path)
    def __call__(self, sentance, bos=True, eos=False):
        out = []
        if bos: out.append(BOS_ID)
        out += self.tokenizer.encode_as_ids(sentance)
        if eos: out.append(EOS_ID)
        return out
    def decode(self, ids):
        ids_list = [int(i.item()) for i in ids]
        return ''.join(self.tokenizer.decode_ids(ids_list))

class SpeechDataset(torch.utils.data.Dataset):

    def __init__(self, text: torch.LongTensor, sizes: torch.LongTensor):
        self.text = text
        self.sizes = sizes

    def __len__(self):
        return len(self.sizes)-1

    def __getitem__(self, index: int):
        return self.text[self.sizes[index]:self.sizes[index+1]]

    @staticmethod
    def collate(batch):
        return torch.nn.utils.rnn.pad_sequence(batch, batch_first=True)


def generatedata(mode: str, encoder, vocab_size: int, sentances):
    datapath = MAINDIR / f"trump-{mode}-{vocab_size}.pth"
    if datapath.is_file():
        return

    text = array.array('L')
    sizes = array.array('L')
    sizes.append(0)
    for s in tqdm(sentances, unit=" sentences"):
        for tokenid in encoder(s, bos=True, eos=True):
            text.append(tokenid)
        sizes.append(len(text))

    data = SpeechDataset(torch.LongTensor(text), torch.LongTensor(sizes))
    with gzip.open(datapath, "wb") as fp:
        torch.save(data, fp)


@click.option("--vocab-size", default=1000, type=int, help="Vocabulary size")
@click.option("--text-path", default="./trump_full_speech.txt", type=str, help="Path to text file")
@click.command()
def cli(vocab_size: int, text_path: str):

    # Création du vocabulaire
    wpmodel = Path("wp_trump{}.model".format(vocab_size))
    if not wpmodel.is_file():
        logging.info("Did not find the wordpiece model %s", wpmodel)
        TRAINPATH = Path("/tmp/trump_speech.txt")
        cleanup(text_path, TRAINPATH)
        program = f"""import sentencepiece as spm; spm.SentencePieceTrainer.Train('--model_prefix=wp_trump{vocab_size} --vocab_size={vocab_size} --input={TRAINPATH} --unk_id=0 --bos_id={BOS_ID} --eos_id={EOS_ID}')"""
        subprocess.run([sys.executable, "-c", program])
        TRAINPATH.unlink()

    # Création des jeux de données
    encoder = Encoder(f"wp_trump{vocab_size}.model")

    sentances = [s for s in datareader(text_path)]
    test_idx = int(.2*len(sentances))
    test  = generatedata("test",  encoder, vocab_size, sentances[:test_idx])
    train = generatedata("train", encoder, vocab_size, sentances[test_idx:])


if __name__ == "__main__":
    cli()
