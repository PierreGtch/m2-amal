import torch
from torch import nn


class RNN_old(nn.Module):
    def __init__(self, inDim, hidenDim):
        super().__init__()
        self.inDim    = inDim
        self.hidenDim = hidenDim
        self.fi = nn.Linear(   inDim, hidenDim, bias=False)
        self.fh = nn.Linear(hidenDim, hidenDim, bias=True)
        self.activation = nn.Sigmoid()

    def one_step(self, x, h):
        x = self.fi(x)
        h = self.fh(h)
        return self.activation(x + h)

    def forward(self, X, h, many_to_many=False): ##X :seq_len*batch*indim, h:batch*hidenDim
        hiddens = [h]
        for i in range(X.shape[0]):
            hiddens.append(self.one_step(X[i], hiddens[i]))
        if many_to_many:
            return torch.stack(hiddens[1:], 0)
        return hiddens[-1]


class RNN(nn.Module):
    def __init__(self, cell):
        super().__init__()
        self.cell = cell

    def forward(self, X, h, many_to_many=False): ##X :seq_len*batch*indim, h:batch*hidenDim
        hiddens = [h]
        for i in range(X.shape[0]):
            hiddens.append(self.cell(X[i], hiddens[i]))
        if many_to_many:
            return torch.stack(hiddens[1:], 0)
        return hiddens[-1]


class RNNgenerator(nn.Module):
    """
    h_t = cell (input_fun x_{t-1}) h_{t-1}
    x_t = output_fun h_t
    """
    def __init__(self, cell, input_fun=None, output_fun=None):
        super().__init__()
        self.cell = cell
        self.input_fun  =  input_fun if  input_fun is not None else nn.Identity()
        self.output_fun = output_fun if output_fun is not None else nn.Identity()

    def forward(self, X, h, length, stopsign=None): ##X :seq_len*batch*indim, h:batch*hidenDim
        hiddens = [h]
        inputs  = [x for x in X]
        shape = inputs[0].shape
        stops = torch.zeros(shape[0], dtype=torch.bool)
        seed_length = len(inputs)
        for i in range(length):
            hiddens.append(self.cell(self.input_fun(inputs[i]), hiddens[i]))
            if i >= seed_length-1:
                inputs.append(self.output_fun(hiddens[i+1]).view(shape))

            if stopsign is not None:
                for i,x in enumerate(inputs[-1]):
                    if x==stopsign:
                        stops[i] = 1
                if torch.all(stops):
                    break

        return torch.stack(inputs, 0)


class BeamSearchGenerator(nn.Module):
    """
    h_t = cell (input_fun x_{t-1}) h_{t-1}
    x_t = beam_search_sampling (log_softmax (output_fun h_t) )
    """
    def __init__(self, cell, num_classes, input_fun=None, output_fun=None):
        super().__init__()
        self.cell = cell
        self.input_fun  =  input_fun if  input_fun is not None else nn.Identity()
        self.output_fun = output_fun if output_fun is not None else nn.Identity()
        self.num_classes = num_classes

    def forward(self, X, h, length, k): ##X :seq_len*indim, h:hidenDim
        """ k : number of sequences to keep at each iteration """
        seq_logP = [torch.zeros(k)]
        hiddens    = [h.repeat(k,1)]
        inputs     = [x.repeat(k,1).view(-1) for x in X]
        seed_length = len(inputs)
        for i in range(length):
            h_t = self.cell(self.input_fun(inputs[i]), hiddens[i])
            next_logP = (nn.functional.log_softmax(self.output_fun(h_t), dim=-1).t() + seq_logP[-1]).t().flatten()
            if i==seed_length-1:
                next_logP = next_logP[:self.num_classes] ## to prevent all sequences to be similar
            _, topk_idx = torch.topk(next_logP, k=k)
            top_seq_idx = topk_idx // self.num_classes
            hiddens.append(h_t[top_seq_idx])
            seq_logP.append(next_logP[topk_idx])
            if i >= seed_length-1:
                inputs = [p[top_seq_idx] for p in inputs]
                inputs.append(topk_idx % self.num_classes)
        return torch.stack(inputs, 1) ## k*seq_len



def ps_elementwise_apply(fn, packed_sequence):
    """applies a pointwise function fn to each element in packed_sequence"""
    return nn.utils.rnn.PackedSequence(fn(packed_sequence.data), packed_sequence.batch_sizes)


##############################################################################
## cells :
##############################################################################

class RNNcell(nn.Module):
    def __init__(self, inDim, hidenDim):
        super().__init__()
        self.inDim    = inDim
        self.hidenDim = hidenDim
        dict = {
            'fi': nn.Linear(   inDim, hidenDim, bias=False),
            'fh': nn.Linear(hidenDim, hidenDim, bias=True),
        }
        self.fn = nn.ModuleDict(modules=dict)
        self.activation = nn.Sigmoid()

    def forward(self, x, h):
        xx = self.fn['fi'](x)
        hh = self.fn['fh'](h)
        return self.activation(xx + hh)

    def get_modules(self):
        return self.fn


class GRUcell(nn.Module):
    def __init__(self, inDim, hidenDim):
        super().__init__()
        self.inDim    = inDim
        self.hidenDim = hidenDim
        dict = {
            'z_h': nn.Linear(hidenDim, hidenDim, bias=False),
            'z_x': nn.Linear(   inDim, hidenDim, bias=False),
            'r_h': nn.Linear(hidenDim, hidenDim, bias=False),
            'r_x': nn.Linear(   inDim, hidenDim, bias=False),
            'W_rh':nn.Linear(hidenDim, hidenDim, bias=False),
            'W_x': nn.Linear(   inDim, hidenDim, bias=False),
        }
        self.fn = nn.ModuleDict(modules=dict)

    def forward(self, x, h):
        z = torch.sigmoid(self.fn['z_h'](h) + self.fn['z_x'](x))
        r = torch.sigmoid(self.fn['r_h'](h) + self.fn['r_x'](x))
        hh = (1-z)*h + z*torch.tanh( self.fn['W_rh'](r*h) + self.fn['W_x'](x) )
        return hh

    def get_modules(self):
        return self.fn


class LSTMcell(nn.Module):
    def __init__(self, inDim, hidenDim):
        super().__init__()
        self.inDim    = inDim
        self.hidenDim = hidenDim
        dict = {
            'f_h': nn.Linear(hidenDim, hidenDim, bias=False),
            'f_x': nn.Linear(   inDim, hidenDim, bias=True ),
            'i_h': nn.Linear(hidenDim, hidenDim, bias=False),
            'i_x': nn.Linear(   inDim, hidenDim, bias=True ),
            'C_h': nn.Linear(hidenDim, hidenDim, bias=False),
            'C_x': nn.Linear(   inDim, hidenDim, bias=True ),
            'o_h': nn.Linear(hidenDim, hidenDim, bias=False),
            'o_x': nn.Linear(   inDim, hidenDim, bias=True ),
        }
        self.fn = nn.ModuleDict(modules=dict)

    def forward(self, x, C_h):
        C, h = C_h
        f = torch.sigmoid(self.fn['f_h'](h) + self.fn['f_x'](x))
        i = torch.sigmoid(self.fn['i_h'](h) + self.fn['i_x'](x))
        o = torch.sigmoid(self.fn['o_h'](h) + self.fn['o_x'](x))
        CC = f*C + i*torch.tanh( self.fn['C_h'](r*h) + self.fn['C_x'](x) )
        hh = o * torch.tanh(CC)
        return (CC,hh)

    def get_modules(self):
        return self.fn
