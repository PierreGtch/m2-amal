import re
from pathlib import Path
import torch
from torch.utils.data import Dataset
from torch.nn.utils.rnn import pack_sequence, pad_sequence, pad_packed_sequence
from datamaestro import prepare_dataset

from bs4 import BeautifulSoup
import string
import unicodedata

EMBEDDING_SIZE = 50



LETTRES = string.ascii_letters+string.digits+' '

class Vocabulary:
    def __init__(self, word2id, oov:bool):
        self.word2id = word2id.copy()
        N = len(self.word2id)
        if set(self.word2id.values()) != set(range(N)):
            raise ValueError
        self.id2word = [None for _ in range(N)]
        for k,v in self.word2id.items():
            self.id2word[v] = k
        self.oov = oov
        if oov:
            self.OOVID = N
            self.id2word.append('__OOV__')
            self.word2id['__OOV__'] = self.OOVID

    def __getitem__(self, i):
        return self.id2word[i]
    def get(self, word: str, add=False):
        try:
            return self.word2id[word]
        except KeyError:
            try:
                return self.word2id[self.normalize(word)]
            except KeyError:
                if add:
                    wordid = len(self.id2word)
                    self.word2id[word] = wordid
                    self.id2word.append(word)
                    return wordid
                if self.oov:
                    return self.OOVID
                raise
    def __len__(self):
        return len(self.id2word)
    @staticmethod
    def normalize(s) :
        return ''.join(c for c in unicodedata.normalize('NFD', s ) if c in LETTRES)

LABELS_VOCAB = Vocabulary({'neg':0, 'pos':1}, oov=False)

class FolderText(Dataset):
    def __init__(self, classes, tokenizer, load=False):
        self.tokenizer = tokenizer
        self.files = []
        self.filelabels = []
        self.labels = list(classes.keys())
        for label, folder in classes.items():
            for file in folder.glob("*.txt"):
                self.files.append(file)
                self.filelabels.append(label)

    def __len__(self):
        return len(self.filelabels)

    def __getitem__(self, ix):
        return self.tokenizer(self.files[ix].read_text()), self.filelabels[ix]



class FolderDataset(Dataset):
    def __init__(self, text:FolderText, vocab:Vocabulary, labels_vocab:Vocabulary):
        data, labels = zip(*iter(text))
        self.data = [torch.tensor(list(map(vocab.get, x))) for x in data]
        self.labels = torch.tensor(list(map(labels_vocab.get, labels)))
        print('data:\t{}\t{}\nlabels:\t{}\t{}\tnum={}'.format(len(self.data), self.data[0].dtype, self.labels.shape, self.labels.dtype, len(labels_vocab)))
    def __len__(self):
        return len(self.labels)
    def __getitem__(self, idx):
        return self.data[idx], self.labels[idx]
    @staticmethod
    def collate(batch):
        data, labels = zip(*sorted(batch, key=lambda x: -len(x[0])))
        return pack_sequence(data), torch.stack(labels, dim=0)



WORDS = re.compile(r"\S+")
def tokenizer(t):
    soup = BeautifulSoup(t, features="lxml")
    return list([x for x in re.findall(WORDS, soup.text.lower())])

train_data_path = 'data/train_tp9.pth'
test_data_path  = 'data/test_tp9.pth'

if __name__=="__main__":
    import gzip

    print('loading text...')
    ds = prepare_dataset("edu.standford.aclimdb")

    print('creating text datasets...')
    train_data = FolderText(ds.train.classes, tokenizer, load=False)
    test_data  = FolderText(ds.test.classes,  tokenizer, load=False)

    print('loading embeddings...')
    word2id, embeddings = prepare_dataset('edu.standford.glove.6b.%d' % EMBEDDING_SIZE).load()
    print('creating Vocabulary...')
    vocab = Vocabulary(word2id, oov=True)

    print('creating id datasets...')
    train_dataset = FolderDataset(train_data, vocab, labels_vocab=LABELS_VOCAB)
    train_data = None
    test_dataset  = FolderDataset( test_data, vocab, labels_vocab=LABELS_VOCAB)
    test_data  = None

    print('saving datasets...')
    with gzip.open(train_data_path, 'wb') as f:
        torch.save(train_dataset, f)
    train_dataset = None
    with gzip.open( test_data_path, 'wb') as f:
        torch.save( test_dataset, f)
    test_dataset = None

    print('loading datasets...')
    with gzip.open(train_data_path, 'rb') as f:
        train_dataset = torch.load(f)
    with gzip.open( test_data_path, 'rb') as f:
        test_dataset  = torch.load(f)

    print('done.')
