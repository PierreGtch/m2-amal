import torch
from torch import nn
from torch.nn import functional as F
from tqdm import tqdm


##############################################################################
#### modules :
##############################################################################

class NN(nn.Module):
    def __init__(self, inSize, outSize, layers=[]):
        super(NN, self).__init__()
        self.layers = nn.ModuleList([])
        for x in layers:
            self.layers.append(nn.Linear(inSize, x))
            inSize = x
        self.layers.append(nn.Linear(inSize, outSize))
    def forward(self, x):
        x = self.layers[0](x)
        for i in range(1, len(self.layers)):
            x = torch.nn.functional.leaky_relu(x)
            x = self.layers[i](x)
        return x


##############################################################################
#### Functions
##############################################################################

def entropie(X, dim=-1):
    P = F.softmax(X, dim=dim)
    return - torch.sum(P * torch.log(P), dim=dim)

def accuracy(y, y_target, dim=-1):
    return (torch.sum(torch.argmax(y.data, dim=dim) == y_target.data), y_target.data.numel())

def accuracy_pad(y, y_target, pad_value=0, dim=-1):
    mask = ~(y_target==pad_value)
    accuracy   = torch.sum((torch.argmax(y, dim=dim)==y_target) * mask.bool()).item()
    all        = torch.sum(mask)
    return accuracy, all

def validation(net, dataloader, loss_fun, accuracy_fun=accuracy):
        with torch.no_grad():
            loss       = 0.
            num_batchs = 0.
            accuracy   = 0.
            all        = 0.
            for x, y_target in tqdm(dataloader, desc='validation'):
                y = net(x)
                l = loss_fun(y.data, y_target.data)
                loss       += float(l)
                num_batchs += 1
                ac, al = accuracy_fun(y, y_target)
                accuracy += float(ac)
                all      += int(al)
            if num_batchs==0 or all==0:
                return None, None
            loss = loss/num_batchs
            accuracy = accuracy/all
            return loss, accuracy

def validation_mask(net, dataloader, loss_fun):
        with torch.no_grad():
            loss       = 0.
            num_batchs = 0.
            accuracy   = 0.
            all        = 0.
            for x, y_target, mask in tqdm(dataloader, desc='validation'):
                y = net(x)
                l = loss_fun(y, y_target, mask)
                loss       += l.item()
                num_batchs += 1
                accuracy   += torch.sum((torch.argmax(y, dim=-1)==y_target) * mask.bool()).item()
                all        += torch.sum(mask)
            if num_batchs==0 or all==0:
                return None, None
            loss /= float(num_batchs)
            accuracy /= float(all)
            return loss, accuracy

def validation_regress(net, dataloader, loss_fun):
        with torch.no_grad():
            loss       = 0.
            num_batchs = 0.
            for x, y_target in tqdm(dataloader, desc='validation'):
                y = net(x)
                l = loss_fun(y, y_target)
                loss       += l.item()
                num_batchs += 1
            if num_batchs==0 or all==0:
                return None
            loss /= float(num_batchs)
            return loss
