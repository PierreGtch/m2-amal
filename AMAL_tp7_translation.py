import torch
from torch import nn
from torch.utils.data import Dataset, DataLoader, random_split
import torch.nn.functional as F
from torch.nn.utils.rnn import pad_packed_sequence
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm
from random import random

from AMAL_tp5 import bcolors
from AMAL_tp7_datasets import VocabularyTrad, TradDataset, PAD, EOS, SOS
from torch_utils import NN, validation, accuracy_pad
from rnn import ps_elementwise_apply, RNN, RNNgenerator

def display(orig_vocab, dest_vocab, x, yt, y):
    to = ['ORIG']      + [ orig_vocab[i.item()] for i in pad_packed_sequence(x)[0][:,0]]
    ta = ['True DEST'] + [ dest_vocab[i.item()] for i in yt[:,0]]
    tp = ['Pred DEST'] + [ dest_vocab[torch.argmax(i,dim=0).item()] for i in y[:,:,0]]
    padding = [orig_vocab[PAD]]*abs(len(to)-len(ta))
    if len(to)>len(ta):
        ta += padding
        tp += padding
    else:
        to += padding
    l0 = len(max(to, key=len))
    l1 = len(max(ta, key=len))
    s = '{{:{}}} {{:{}}} {{}}'.format(l0,l1)
    for x in zip(to,ta,tp):
        if x[1]!=x[2]:
            print((bcolors.FAIL+s+bcolors.ENDC).format(*x))
        else:
            print(s.format(*x))

class TradNet(nn.Module):
    def __init__(self, inDim, embedDim, hiddenDim, fc_layers, outDim):
        super().__init__()
        self.hiddenDim = hiddenDim
        self.outDim = outDim

        self.embedding_source = nn.Embedding(inDim,  embedDim)
        self.embedding_dest   = nn.Embedding(outDim, embedDim)
        self.rnn_read  = nn.GRU(input_size=embedDim, hidden_size=hiddenDim, bidirectional=False)
        self.fc = NN(hiddenDim, outDim, fc_layers)
        self.rnn_write_cell = nn.GRUCell(input_size=embedDim, hidden_size=hiddenDim)
        self.rnn_write_constr = RNN(self.rnn_write_cell)
        #self.rnn_write_non_c  = RNNgenerator(rnn_write_cell, input_fun=self.embedding_dest, output_fun=self.fc)

    def forward(self, x, y_target=None):      ## x : pack_sequence
        """ if y_target is specified, the network will be in constrained mode
            if y_target=None: non-constrained mode """
        bs = x.batch_sizes[0]
        h = - torch.ones((1, bs, self.hiddenDim))
        x = ps_elementwise_apply(self.embedding_source, x)
        _,h_n = self.rnn_read(x, h)
        h_n = h_n[0]
        if y_target is not None: ## constarined_mode
            yt = torch.cat((torch.zeros([1,*y_target.shape[1:]], dtype=y_target.dtype)+SOS, y_target[:-1]), dim=0)
            y = self.embedding_dest(yt)
            y = self.rnn_write_constr(y, h_n, many_to_many=True)
            y = self.fc(y)
        else:
            init = torch.zeros((bs,self.outDim))
            init[:,SOS] = 1.
            inputs  = [init]
            shape = inputs[0].shape
            stops = torch.zeros(shape[0], dtype=torch.bool)
            for i in range(100):
                h_n = self.rnn_write_cell(self.embedding_dest(torch.argmax(F.softmax(inputs[i], dim=-1), dim=-1)), h_n)
                inputs.append(self.fc(h_n).view(shape))

                for i,x in enumerate(inputs[-1]):
                    if torch.argmax(x,dim=-1)==EOS:
                        stops[i] = 1
                if torch.all(stops):
                    break

            y = torch.stack(inputs[1:], 0)
        return y.transpose(-1,-2)

def resize_to_target(y, y_target):
        st = y_target.shape[0]
        s  =        y.shape[0]
        if st==s:
            return y
        elif st<s:
            return y[:st]
        s2 = [st-s]+list(y.shape[1:])
        return torch.cat((y, torch.zeros(s2, dtype=y.dtype)), dim=0)

class LossPad(nn.Module):
    def __init__(self):
        super().__init__()
        self.loss = nn.CrossEntropyLoss(ignore_index=PAD)
    def forward(self, y, y_target):
        y2 = resize_to_target(y, y_target)
        return self.loss(y2, y_target)

accuracy_fun = lambda y, y_target: accuracy_pad(resize_to_target(y,y_target), y_target, pad_value=PAD, dim=1)

if __name__=="__main__":

    with open("data/fra.txt") as f:
        lines = f.read()
    vocEng = VocabularyTrad()
    vocFra = VocabularyTrad()
    data = TradDataset(lines,vocEng,vocFra, max_len=10)
    l = len(data)
    train_data, test_data = random_split(data, [int(l*.8), l-int(l*.8)])

    train_batchsize = 100
    test_batchsize  = 300
    train_dataloader = DataLoader(train_data, collate_fn=TradDataset.collate, shuffle=True, batch_size=train_batchsize)
    test_dataloader  = DataLoader( test_data, collate_fn=TradDataset.collate, shuffle=True, batch_size= test_batchsize)

    inDim    = len(vocEng)
    outDim   = len(vocFra)
    embedDim  = 50
    hiddenDim = 50
    fc_layers = [50]
    print('inDim={}, outDim={}'.format(inDim, outDim))
    net = TradNet(inDim=inDim, embedDim=embedDim, hiddenDim=hiddenDim, outDim=outDim, fc_layers=fc_layers)
    optimizer = torch.optim.Adam(params=net.parameters()) #, lr=0.005)

    loss_fun = LossPad()

    # x,yt = next(iter(train_dataloader))
    # y = net(x)
    # print(loss_fun(y,yt), yt.shape, y.shape)
    # print("val ", validation(net, test_dataloader, loss_fun, accuracy_fun))
    # print("non const ", loss_fun(net(x), yt))
    # print("const ", loss_fun(net(x, yt), yt))
    # exit(0)

    ## display params :
    N = 10000
    loss_freq      = 1 # in number of epochs
    histogram_freq = 1
    test_freq      = 3
    display_freq   = 20
    p_const = .8
    p_const_decay = .99

    ## Savers :
    writer = SummaryWriter()

    ## Training loop :
    for i in tqdm(range(N), desc='epochs'):
        p_const *= p_const_decay
        loss_train = 0.
        num_batchs = 0.
        for j, (x, y_target) in enumerate(tqdm(train_dataloader, desc='train')):
        ## train (one epoch) :
            optimizer.zero_grad()
            net.zero_grad()
            if random()<p_const: ## constrained
                y = net(x, y_target=y_target)
            else:                ## not constrained
                y = net(x)
            loss = loss_fun(y, y_target)
            loss.backward()
            optimizer.step()
            loss_train += loss.item()
            num_batchs += 1

        if i%display_freq==0:
            display(vocEng, vocFra, x, y_target, resize_to_target(y, y_target))

        if i%loss_freq==0:
            loss_train /= num_batchs
            writer.add_scalars('loss', {'train': loss_train}, i)

        ## test network :
        if i%test_freq==0:
            loss_test, accuracy_test = validation(net, test_dataloader, loss_fun, accuracy_fun)
            writer.add_scalars('loss',     {'test':     loss_test}, i)
            writer.add_scalars('accuracy', {'test': accuracy_test}, i)
            writer.flush()

    writer.close()
