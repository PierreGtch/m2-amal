import torch
import torch.nn as nn
from torch.nn import functional as F
import numpy as np
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import Dataset, DataLoader, random_split
from torch.nn.utils.rnn import pack_sequence, pad_sequence, pad_packed_sequence
from tqdm import tqdm
from datamaestro import prepare_dataset

from tp9_preprocess import FolderDataset, train_data_path, test_data_path, LABELS_VOCAB, EMBEDDING_SIZE, Vocabulary
from torch_utils import NN, validation, accuracy, entropie

from rnn import ps_elementwise_apply

class Displayer():
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    def __init__(self, vocab, labels_vocab):
        self.vocab = vocab
        self.labels_vocab = labels_vocab
    def __call__(self, x, y, y_target, interval=None):
        t = list(map(self.vocab.__getitem__, list(x)))
        l = len(t)
        correct = (torch.argmax(y)==y_target).item()
        if interval is not None:
            t.insert(min(l,interval[1]), Displayer.ENDC)
            t.insert(min(l,interval[0]), Displayer.OKBLUE if correct else Displayer.WARNING)
        sentance = ' '.join(t)
        prefix = (Displayer.OKGREEN+'CORRECT ' if correct else Displayer.FAIL+'WRONG   ')+ self.labels_vocab[y_target]+Displayer.ENDC
        print("{} -> {}".format(prefix, sentance))


##################################################################################

def get_embedding_module(embedding):
    oov_embedding = torch.cat([torch.tensor(embedding).float(), torch.zeros((1, EMBEDDING_SIZE))], dim=0)
    return nn.Embedding.from_pretrained(oov_embedding, freeze=True)

def packed_sequence_average(x):
    x, lengths = pad_packed_sequence(x)
    return torch.sum(x, dim=0) / lengths.view(-1,1).float()

class Net0(nn.Module):
    def __init__(self, embedding, outDim, fc_layers):
        super().__init__()
        self.embedding = embedding
        self.fc = NN(inSize=EMBEDDING_SIZE, outSize=outDim, layers=fc_layers)
    def forward(self, x):
        x = ps_elementwise_apply(self.embedding, x)
        x = packed_sequence_average(x)
        return self.fc(x)

class Net1(nn.Module):
    def __init__(self, embedding, outDim, fc_layers):
        super().__init__()
        self.embedding = embedding
        self.attention = nn.Linear(EMBEDDING_SIZE, 1)
        self.fc = NN(inSize=EMBEDDING_SIZE, outSize=outDim, layers=fc_layers)
    def forward(self, x):
        x = ps_elementwise_apply(self.embedding, x)
        p = ps_elementwise_apply(self.attention, x)
        p, _ = pad_packed_sequence(p)
        p = F.softmax(p, dim=0)
        x, _ = pad_packed_sequence(x)
        return self.fc(torch.sum(x * p, dim=0))


class Net2(nn.Module):
    def __init__(self, embedding, outDim, fc_layers):
        super().__init__()
        self.embedding = embedding
        self.attention = nn.Linear(EMBEDDING_SIZE, EMBEDDING_SIZE)
        self.representation = NN(inSize=EMBEDDING_SIZE, outSize=EMBEDDING_SIZE, layers=fc_layers)
        self.fc = NN(inSize=EMBEDDING_SIZE, outSize=outDim, layers=fc_layers)
    def forward(self, x):
        x = ps_elementwise_apply(self.embedding, x)
        t = packed_sequence_average(x) #batch*embed
        q = self.attention(t)  #batch*embed
        x, _ = pad_packed_sequence(x)  #seq*batch*embed
        p = torch.sum(x*q, dim=-1, keepdim=True) # seq*batch*1
        p = F.softmax(p, dim=0) # seq*batch*1
        return self.fc(torch.sum(self.representation(x) * p, dim=0))



##################################################################################


if __name__=='__main__':
    import gzip

    batch_size = 100
    test_batch_size = 50

    print('loading datasets...')
    with gzip.open(train_data_path, 'rb') as f:
        dataset = torch.load(f)
    with gzip.open( test_data_path, 'rb') as f:
        test_dataset  = torch.load(f)

    l = len(dataset)
    train_dataset, val_dataset = random_split(dataset, [l//2, l-l//2])
    train_dataloader = DataLoader(train_dataset, collate_fn=FolderDataset.collate, shuffle=True,  batch_size=batch_size)
    val_dataloader   = DataLoader(  val_dataset, collate_fn=FolderDataset.collate, shuffle=False, batch_size=test_batch_size)
    test_dataloader  = DataLoader( test_dataset, collate_fn=FolderDataset.collate, shuffle=False, batch_size=test_batch_size)
    print('\ndatasets lengths :\ttrain={}, val={}, test={}\n'.format(len(train_dataset), len(val_dataset), len(test_dataset)))
    print('loading embeddings...')
    word2id, embeddings_matrix = prepare_dataset('edu.standford.glove.6b.%d' % EMBEDDING_SIZE).load()
    embedding_layer = get_embedding_module(embeddings_matrix)
    vocab = Vocabulary(word2id, oov=True)
    display = Displayer(vocab, labels_vocab=LABELS_VOCAB)

    ## net params :
    outDim = 2
    layers = [100]
    #net_name, net = 'net0', Net0(embedding_layer, outDim=outDim, fc_layers=layers)
    #net_name, net = 'net1', Net1(embedding_layer, outDim=outDim, fc_layers=layers)
    net_name, net = 'net2', Net2(embedding_layer, outDim=outDim, fc_layers=layers)
    optimizer = torch.optim.Adam(params=net.parameters())#, lr=1e-4)
    loss_fun = nn.CrossEntropyLoss()

    # x,yt = next(iter(train_dataloader))
    # y=net(x)
    # print(x.data.shape, y.shape, yt.shape)
    # #print(x, y, yt)
    # l = loss_fun(y,yt)
    # #print(y,yt,l)
    # xx, xlengths = pad_packed_sequence(x)
    # for _ in range(5):
    #     idx = np.random.randint(y.shape[0])
    #     x0 = xx[:xlengths[idx], idx]
    #     display(x0, y[idx], yt[idx])
    # exit()

    ## display params :
    N = 1000
    loss_freq      = 1 # in number of epochs
    histogram_freq = 3
    test_freq      = 1
    display_freq   = 2

    ## Savers :
    writer = SummaryWriter(comment='_tp9_'+net_name)

    ## Training loop :
    for i in tqdm(range(N), desc='epochs'):
        loss_train, num_batchs, acc_train, num_elem = [torch.zeros(()) for _ in range(4)]
        for j, (x, y_target) in enumerate(tqdm(train_dataloader, desc='train')):
        ## train (one epoch) :
            optimizer.zero_grad()
            net.zero_grad()
            net.train()
            y = net(x)
            loss = loss_fun(y, y_target)
            loss.backward()
            optimizer.step()
            acc, total = accuracy(y, y_target)
            loss_train += loss
            num_batchs += 1
            acc_train  += acc
            num_elem   += total

        if i%loss_freq==0:
            writer.add_scalars('loss',     {'train': loss_train/num_batchs}, i)
            writer.add_scalars('accuracy', {'train': acc_train/num_elem},    i)

        if i%display_freq==0:
            xx, xlengths = pad_packed_sequence(x)
            for _ in range(1):
                idx = np.random.randint(y.shape[0])
                x0 = xx[:xlengths[idx],idx]
                display(x0, y[idx], y_target[idx])

        ## histograms :
        # if i%histogram_freq==0:
        #     for j,(l,o) in enumerate(zip(net.layers, net.outputs)):
        #         writer.add_histogram('weight/layer_'+str(j), l.weight,      i)
        #         writer.add_histogram(  'grad/layer_'+str(j), l.weight.grad, i)
        #         writer.add_histogram(  'output/layer_'+str(j),o, i)
        #         writer.flush()


        ## test network :
        if i%test_freq==0:
            loss_test, acc = validation(net, val_dataloader, loss_fun)
            writer.add_scalars('loss',     {'test': loss_test}, i)
            writer.add_scalars('accuracy', {'test': acc}, i)
            writer.flush()

    writer.close()
