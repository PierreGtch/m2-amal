import torch
from torch import nn
from torch.utils.data import Dataset, DataLoader, random_split
import numpy as np
import pandas as pd
from tqdm import tqdm
from rnn import RNN, RNNgenerator, RNNcell, GRUcell, LSTMcell, BeamSearchGenerator
from torch_utils import  NN, validation_mask
from AMAL_tp4_gen import SoftmaxSample
from trump_speech_preprocess import SpeechDataset, Encoder



class SequenceGenerator(nn.Module):
    def __init__(self, inDim, cell, fc_layers, outDim, k_beam_search, encoder):
        super().__init__()
        self.encoder = encoder
        embedDim = cell.inDim
        hidenDim = cell.hidenDim
        self.inDim, self.embedDim, self.hidenDim = inDim, embedDim, hidenDim
        self.k_beam_search = k_beam_search

        self.embedding = nn.Embedding(inDim, embedDim)
        self.rnn = RNN(cell)
        self.fc = NN(hidenDim, outDim, fc_layers)
        self.generator_bs = BeamSearchGenerator(cell,
            input_fun=self.embedding,
            output_fun=self.fc,
            num_classes=outDim,
        )
        self.generator = RNNgenerator(cell,
            input_fun=self.embedding,
            output_fun=nn.Sequential(self.fc, SoftmaxSample()),
        )

    def forward(self, x):      ## x : batch*seq_len*inDim
        h = - torch.ones(self.hidenDim)
        x = self.embedding(x)
        x = x.view(x.shape[0], x.shape[1], self.embedDim).transpose(0,1)  ## now x : seq_len*batch*inDim
        x = self.rnn(x, h, many_to_many=True)
        x = self.fc(x) ## x : seq_len*batch*inDim
        return x.transpose(1,0)

    def generate(self, initial_st, length): ## initial : seq_len*inDim
        initial = torch.tensor(self.encoder(initial_st, bos=True, eos=False))
        h0 = - torch.ones(self.hidenDim)

        seqs = self.generator_bs(initial, h0, length, self.k_beam_search)
        print('beam search :')
        for seq_num in range(num_to_display):
            print('{}\t{}'.format(seq_num, self.encoder.decode(seqs[seq_num])))
        seq2 = self.generator(initial, h0, length)
        print('multimodal sample :\n\t{}:'.format(self.encoder.decode(seq2)))

#SIMILAR TO nn.CrossEntropyLoss(ignore_index=<PAD_IDX>)
## loss adapted to the sequence padding  :
class MaskedCrossEntropyLoss(nn.Module):
    def forward(self, y, target, mask): ## y: batch*C*k1*k2*...
        # print(y.shape, target.shape, mask.shape)
        # print(y,"lsm", nn.functional.log_softmax(y))
        # print(mask, y + (mask.unsqueeze(-1) + 1e-45).log())
        #yy = nn.functional.log_softmax(y + (mask.unsqueeze(-1) + 1e-49).log(), dim=-1).transpose(1,-1)
        d = y.dim()
        p = [0, d-1]+list(range(1,d-1))
        yy = nn.functional.log_softmax(y.permute(p) , dim=1)
        # print(yy, yy2)
        # print(nllloss(yy,target).shape, mask.shape)
        # print(nllloss(yy,target), "\n\n",nllloss(yy2,target),'\n\n',mask)
        # print(torch.sum(nllloss(yy, target) * mask), torch.sum(mask))
        # exit(0)
        return torch.sum(nn.functional.nll_loss(yy, target, reduction='none') * mask) / torch.sum(mask)

class ShiftSeqDataset(Dataset):
    def __init__(self, ds):
        self.ds = ds
    def __len__(self):
        return self.ds.__len__()
    def __getitem__(self, idx):
        d = self.ds.__getitem__(idx)
        mask = torch.ones(len(d)-1)
        return d[:-1], d[1:], mask
    @staticmethod
    def collate(batch):
        x, y, mask = zip(*batch)
        return (
            torch.nn.utils.rnn.pad_sequence(x,    batch_first=True),
            torch.nn.utils.rnn.pad_sequence(y,    batch_first=True),
            torch.nn.utils.rnn.pad_sequence(mask, batch_first=True),
            )



##############################################################################
#### main
##############################################################################

if __name__=="__main__":
    ## imports :
    from torch.utils.tensorboard import SummaryWriter
    import os
    import pickle as pkl
    import gzip

    ## load dataset temperatures :
    path="./data/trump_full_speech.txt"
    train_batch = 50
    test_batch  = 100


    torch.manual_seed(12)

    ## Load data :
    print('loading data..')
    num_tokens = 1000
    train_path = "data/trump-train-{}.pth".format(num_tokens)
    test_path  =  "data/trump-test-{}.pth".format(num_tokens)
    model_path =   "data/wp_trump{}.model".format(num_tokens)
    encoder = Encoder(model_path)
    with gzip.open(train_path, 'rb') as f:
        train_ds = ShiftSeqDataset(torch.load(f))
    with gzip.open( test_path, 'rb') as f:
        test_ds  = ShiftSeqDataset(torch.load(f))
    train_dataloader = DataLoader(train_ds, collate_fn=ShiftSeqDataset.collate, shuffle=True, batch_size=train_batch)
    test_dataloader  = DataLoader( test_ds, collate_fn=ShiftSeqDataset.collate, shuffle=True, batch_size= test_batch)
    print('data loaded')


    ## network definition :
    k_beam_search = 100
    num_to_display = 5
    inDim = num_tokens # encodage one_hot des caractères
    outDim = inDim
    embedDim = 50
    hidenDim = 100#2*outDim
    fc_layers = [] #[2*outDim]
    cells = (
        ('GRU',   GRUcell),
        #('LSTM', LSTMcell),
        #('RNN',   RNNcell),
    )
    nets = {}
    for cell_n, C in cells:
        celll = C(embedDim, hidenDim)
        nnet = SequenceGenerator(inDim=inDim, cell=celll, outDim=outDim, fc_layers=fc_layers, k_beam_search=k_beam_search, encoder=encoder)
        opt = torch.optim.Adam(params=nnet.parameters()) #, lr=0.005)
        #opt = torch.optim.SGD(params=net.parameters() , lr=.01, momentum=.9)
        nets[cell_n] = (nnet, opt)

    loss_func = MaskedCrossEntropyLoss()
    #loss_func = lambda y,yt,m: nn.functional.cross_entropy(y.transpose(-1,1),yt)

    ## display params :
    N = 30
    loss_freq      = 1 # in number of epochs
    save_model_freq= 1
    histogram_freq = 1
    test_freq      = 1
    generate_freq  = 1

    ## Savers :
    writer = SummaryWriter()

    ## generate  a sequence
    with torch.no_grad():
        for name,(net, _) in nets.items():
            print('\n{}:'.format(name))
            net.generate('I, Donald Trump', 90)


    ## Training loop :
    for i in tqdm(range(N), desc='epochs'):
        for cell_name, (net, optimizer) in nets.items():
            ## train (one epoch) :
            optimizer.zero_grad()
            loss_train = 0.
            num_batchs = 0.
            for x, y_target, mask in tqdm(train_dataloader, desc='train'):
                y = net(x)
                loss = loss_func(y, y_target, mask)
                loss.backward()
                optimizer.step()
                loss_train += loss.item()
                num_batchs += 1

            if i%loss_freq==0:
                loss_train /= num_batchs
                writer.add_scalars('loss',     {'{}/train'.format(cell_name):     loss_train}, i)

            ## test network :
            if i%test_freq==0 or i%save_model_freq==0:
                loss_test, accuracy_test = validation_mask(net, test_dataloader, loss_func)
                writer.add_scalars('loss',     {'{}/test'.format(cell_name):     loss_test}, i)
                writer.add_scalars('accuracy', {'{}/test'.format(cell_name): accuracy_test}, i)
                writer.flush()

            if i%histogram_freq==0:
                ## On enregistre les gradients à différentes couches pour constater si ils se propagent bien ou non :
                for m_name,m in net.rnn.cell.get_modules().items():
                    writer.add_histogram('weights/{}/{}'.format(cell_name,m_name), m.weight,      i)
                    writer.add_histogram(  'grads/{}/{}'.format(cell_name,m_name), m.weight.grad, i)
                writer.add_histogram(       'outputs/{}'.format(cell_name),        y,             i)

            if i%generate_freq==0:
                with torch.no_grad():
                    print('\n{} :'.format(cell_name))
                    net.generate('I, Donald Trump', 90)

    writer.close()


# GRU iter #20
# k_beam_search = 100
# embedDim = 50
# hidenDim = 100#2*outDim
# beam search :
# 0	I, Donald Trump.EOSEOS happen to happen from that siding that happend that happen to happen to
# 1	I, Donald Trump.EOSEOS happen to happen from that siding that happen to happen from that spent
# 2	I, Donald Trump.EOSEOS happen to happen from that siding that happen to have from that spent th
# 3	I, Donald Trump.EOSEOS happen to happen from that siding that happen to have from that spents h
# 4	I, Donald Trump.EOSEOS happen to happen from that siding that happend that happen to happen fro
# multimodal sample :
# 	I, Donald Trump to happt, has goverman fire duds hame sciss, itss, scos ancly, mrings Satre:
